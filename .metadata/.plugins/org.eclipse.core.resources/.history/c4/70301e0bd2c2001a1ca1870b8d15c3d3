package selenium;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * What is a dynamic element on the web page? A dynamic elements is an element
 * which has dynamic attributes and/or dynamic location on the web page.
 * 
 * How to locate the dynamic elements on the web page? We can locate dynamic
 * elements using: (a) Absolute xpath
 * /html[1]/body[1]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[2]/div[2]/div[2]/ul[1]/li[1]
 * 
 * (b) Relative Xpath using Functions and Axes (i) Xpath Functions
 * //a[starts-with(@href, "https://play.google.com/")] //a[contains(@href,
 * "https://play.google.com/")] //span[text() = "Play"] (ii) Xpath Axes
 * //a[starts-with(@href, "https://play.google.com/")]/parent::li //span[text()
 * = "Play"]/preceding-sibling::span (c) CSS (refer:
 * https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors) (i) Simple
 * selectors using wildcard //a[href^="https://play.google.com/"]
 * ----starts-with //a[href*="https://play.google.com/"] ----contains (ii)
 * Multiple Selectors //[class="j1ei8c"] [data-pid="78"] (iii) Combinators
 * //Child combinator (A>B) //a[href*="play.google"]>span.Rq5Gcb ---- gets
 * direct child //li.j1ei8c a[href*="play.google"] ---- gets all grand children
 * by the white space and then gets a given grand child //Descendant combinator
 * (A B) //ul.LVal7b.u4RcUd .j1ei8c a[href*="play.google"] ----- //Adjacent
 * sibling combinator (A+B) //ul.gb_da.gb_6+a
 * 
 * ------ Xpath Reference ------- XPath - Reference Absolute XPath: Absolute
 * XPath begins from the root element "/" and traverses through the DOM tree
 * until it reaches the desired element. Note that if the DOM tree structure
 * above the desired element changes in future, the absolute path will not work.
 * Example:/html[1]/body[1]/div[1]/div[4]/div[1]/div[2]/div[1]/div[1]/div[
 * 2]/div[2]/div[2]/ul[1]/li[1] Begins at the root element / and traverses until
 * it reaches the desired element li RelativeXPath: Relative XPath originates
 * anywhere in the DOM tree beginning with whichever element you want using
 * "//". Example: //a[@id="gb78"] //a[@id="gb78"]/span Begins at the specified
 * element a[@id="gb78"] XPath Functions: starts-with() The starts-with checks
 * whether the value of attribute(first argument) starts with the given
 * value(second argument) and returns true or false. Example:
 * //a[starts-with(@href, "https://play.google.com/")] //*[starts-with(@href,
 * "https://play.google.com/")] Checks if the href attribute value starts with
 * "https://play.google.com/" and returns true or false. contains() The contains
 * checks whether the value of attribute(first argument) contains anywhere the
 * given value(second argument) and returns true or false. Example:
 * //a[contains(@href, "play.google.com/")] Checks if the href attribute value
 * contains "https://play.google.com/" and returns true or false. text() The
 * text checks whether the inner text matches the given value and returns true
 * or false. Example: //span[text()="Play"] Checks if the span element has inner
 * text that matches "Play" and returns true or false. XPath Axes:
 * preceding-sibling Indicates all the nodes that have the same parent as the
 * context node and appear before the context node in the source document.
 * Example: //span[text() = "Play"]/preceding-sibling::span Locates the span
 * element that precedes the element //span[text() = "Play"] and have the same
 * parent. following-sibling Indicates all the nodes that have the same parent
 * as the context node and appear after the context node in the source document.
 * Example: //a[text()="More"]/following-sibling::a Locates the a element that
 * follows the element //a[text()="More"] and have the same parent. parent
 * Indicates the single node that is the parent of the context node. It can be
 * abbreviated as two periods (..). Example: //a[starts-with(@href,
 * "https://play.google.com/")]/parent::li //a[starts-with(@href,
 * "https://play.google.com/")]/.. Locates the parent for //a[starts-with(@href,
 * "https://play.google.com/")]. ancestor Indicates the nodes that are
 * ancestors(parent, grandparent and more) of the context node. Example:
 * //a[starts-with(@href, "https://play.google.com")]/ancestor::ul Locates the
 * ancestor ul element for "//a[starts-with(@href,
 * "https://play.google.com/")]". child Indicates the direct children of the
 * context node. Example: //a[starts-with(@href,
 * "https://play.google.com/")]/child::span Locates the direct children span for
 * //a[starts-with(@href, "https://play.google.com/")]. descendant Indicates all
 * the descendants(children, grandchildren and more) of the context node.
 * Example: //a[starts-with(@href, "https://play.google.com/")]/descendant::span
 * Locates all the descendant span for //a[starts-with(@href,
 * "https://play.google.com/")]. For Further Reading:
 * https://developer.mozilla.org/en-US/docs/Web/XPath
 * 
 * ------- CSS Selector Reference ----------- CSS Reference I. Simple selectors:
 * ID selector
 * 
 * Selects elements based on the value of the id attribute.
 * 
 * Syntax: #idname Example: #gb78 will match the elements with ID gb78.
 * 
 * 
 * 
 * Class selector
 * 
 * Selects elements that have the given class attribute. Syntax: .classname
 * Example: .gb_0 will match the elements with class gb_0.
 * 
 * 
 * 
 * Attribute selector
 * 
 * Selects elements that have the given attribute or based on the value of the
 * given attribute. Syntax: [attr] [attr=value] Example: [href] will match the
 * elements having href attribute set (to any value).
 * 
 * [href=�https://play.google.com/?hl=en&tab=w8�] will match elements having
 * href attribute set to given value.
 * 
 * 
 * 
 * Universal selector
 * 
 * Selects all elements. Optionally, it may be restricted to a specific
 * namespace or to all namespaces. Syntax: *
 * 
 * ns|* Example: * will match all the elements of the document.
 * 
 * 
 * 
 * II. Wildcards: [attr^=value]Represents elements with an attribute name of
 * attr whose value is prefixed (preceded) by value.
 * 
 * Example: [href^="https://play.google.com/"]
 * 
 * a[href^="https://play.google.com/"]
 * 
 * 
 * 
 * [attr*=value]Represents elements with an attribute name of attr whose value
 * contains at least one occurrence of value within the string.
 * 
 * Example: [href*="play.google.com"]
 * 
 * a[href*="play.google.com"]
 * 
 * 
 * 
 * [attr$=value]Represents elements with an attribute name of attr whose value
 * is suffixed (followed) by value.
 * 
 * Example: [href$="hl=en&tab=w8"]
 * 
 * a[href$="hl=en&tab=w8"]
 * 
 * 
 * 
 * [attr~=value]Represents elements with an attribute name of attr whose value
 * is a whitespace-separated list of words, one of which is exactly value.
 * 
 * Example: [title~="apps"]
 * 
 * a[title~="apps"]
 * 
 * 
 * 
 * III. Multiple Selectors: [attr1=value][attr2=value]Represents elements with
 * attributes of attr1 AND attr2 along with given values.
 * 
 * Example: [class="gb_O"][data-pid="78"]
 * 
 * 
 * 
 * IV. Combinators: Child combinator
 * 
 * The > combinator selects nodes that are direct children of the first element.
 * Syntax: A > B Example: a[href*="play.google"]>span.gb_W will match all
 * span.gb_W elements that are nested directly inside a a[href*="play.google"]
 * element.
 * 
 * 
 * 
 * Descendant combinator
 * 
 * The (space) combinator selects nodes that are descendants(children,
 * grandchildren and more) of the first element. Syntax: A B Example:
 * ul.gb_da.gb_6 a[href*="play.google"] will match all a[href*="play.google"]
 * elements that are inside a ul.gb_da.gb_6 element.
 * 
 * 
 * 
 * Adjacent sibling combinator
 * 
 * The + combinator selects adjacent siblings. This means that the second
 * element directly follows the first, and both share the same parent. Syntax: A
 * + B Example: ul.gb_da.gb_6+a will match all a elements that directly follow
 * an ul.gb_da.gb_6.
 * 
 * 
 * 
 * General sibling combinator
 * 
 * The ~ combinator selects siblings. This means that the second element follows
 * the first (though not necessarily immediately), and both share the same
 * parent. Syntax: A ~ B Example: ul.gb_da.gb_6~ul will match all ul elements
 * that follow a ul.gb_da.gb_6.
 * 
 * 
 * 
 * For Further Reading:
 * https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors
 */

public class LocateDynamicWebElements {

	public static void main(String[] args) throws InterruptedException {
//		System.setProperty("webdriver.gecko.driver", "C:\\\\WebDrivers\\\\FirefoxDriver\\\\geckodriver-v0.26.0-win64\\\\geckodriver.exe");
//		WebDriver driver = new FirefoxDriver();
//		driver.get("http://www.google.com");

		System.setProperty("webdriver.chrome.driver", "C:\\WebDrivers\\ChromeDriver\\v83\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.google.com");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		WebDriverWait wait = new WebDriverWait(driver,30);

		driver.findElement(By.id("cnsd")).click();
		driver.findElement(By.cssSelector("a[title='Google Apps']")).click();
		driver.findElement(By.cssSelector("a[title='Google Apps']")).getAttribute("aria-expanded").contentEquals("true");
		driver.findElement(By.cssSelector("#gbw > div > div > div:nth-child(3) > iframe")).getAttribute("style").contentEquals("height: 100%; width: 100%;");
//		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector("a[href*='play.google.com']"))));
		Actions action = new Actions(driver);
		action.click(driver.findElement(By.cssSelector("a[title='Google Apps']"))).build().perform();
		driver.findElement(By.cssSelector("a[href*='play.google.com']")).click();

		//driver.navigate().back();
		driver.findElement(By.xpath("//a[@title='Google Apps']")).click();
		driver.findElement(By.xpath("//html/body/div/c-wiz/div/div/c-wiz/div/div/ul[1]/li[5]/a")).click();
		
		driver.quit();
	}

}
